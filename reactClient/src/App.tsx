import React from 'react';
import logo from './logo.svg';
import './App.css';
import { useQuery, gql } from '@apollo/client';
import { Query } from '../src/schema'

interface IProps {
  id: string;
  name: string;
  bio: string;
}

function App() {
  const GETT = gql`
  query{ 
    speakers {
    nodes {
      name bio id
    }
  }
}
  `;
  function DisplayLocations() {
    console.log(GETT);
    // const { loading, error, data } = useQuery(GETT);
    const { loading, error, data } = useQuery<Query>(GETT);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>Error :(</p>;

    return (
      <>
        {data?.speakers?.nodes?.map(({ id, name, bio }) => (
          <div key={id}>
            <h3>{name}</h3>
            <br />
            <b>About speaker:</b>
            <p>{bio}</p>
            <p>{id}</p>
            <br />
          </div>
        ))}
      </>
    );

  }
  return (
    <div className="App">
      <h2>My first Apollo app 🚀</h2>
      <br />
      <DisplayLocations />
    </div>
  );
}

export default App;
