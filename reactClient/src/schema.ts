import { gql } from "@apollo/client";
import * as Apollo from "@apollo/client";
export type Maybe<T> = T | null;
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K];
};
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]?: Maybe<T[SubKey]>;
};
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & {
  [SubKey in K]: Maybe<T[SubKey]>;
};
const defaultOptions = {};
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string;
  String: string;
  Boolean: boolean;
  Int: number;
  Float: number;
  /** The `DateTime` scalar represents an ISO-8601 compliant date time type. */
  DateTime: any;
  /** The `TimeSpan` scalar represents an ISO-8601 compliant duration type. */
  TimeSpan: any;
};

export type Query = {
  __typename?: "Query";
  /** Fetches an object given its ID. */
  node?: Maybe<Node>;
  /** Lookup nodes by a list of IDs. */
  nodes: Array<Maybe<Node>>;
  /** Gets all attendees of this conference. */
  attendees?: Maybe<AttendeesConnection>;
  /** Gets an attendee by its identifier. */
  attendeeById: Attendee;
  attendeesById: Array<Attendee>;
  sessions?: Maybe<SessionsConnection>;
  sessionById: Session;
  sessionsById: Array<Track>;
  speakers?: Maybe<SpeakersConnection>;
  speakerById: Speaker;
  speakersById: Array<Speaker>;
  tracks?: Maybe<TracksConnection>;
  trackByName: Track;
  trackByNames: Array<Track>;
  trackById: Track;
};

export type QueryNodeArgs = {
  id: Scalars["ID"];
};

export type QueryNodesArgs = {
  ids: Array<Scalars["ID"]>;
};

export type QueryAttendeesArgs = {
  first?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["String"]>;
};

export type QueryAttendeeByIdArgs = {
  id: Scalars["ID"];
};

export type QueryAttendeesByIdArgs = {
  ids: Array<Scalars["ID"]>;
};

export type QuerySessionsArgs = {
  first?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["String"]>;
  where?: Maybe<SessionFilterInput>;
  order?: Maybe<Array<SessionSortInput>>;
};

export type QuerySessionByIdArgs = {
  id: Scalars["ID"];
};

export type QuerySessionsByIdArgs = {
  ids: Array<Scalars["ID"]>;
};

export type QuerySpeakersArgs = {
  first?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["String"]>;
};

export type QuerySpeakerByIdArgs = {
  id: Scalars["ID"];
};

export type QuerySpeakersByIdArgs = {
  ids: Array<Scalars["ID"]>;
};

export type QueryTracksArgs = {
  first?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["String"]>;
};

export type QueryTrackByNameArgs = {
  name: Scalars["String"];
};

export type QueryTrackByNamesArgs = {
  names: Array<Scalars["String"]>;
};

export type QueryTrackByIdArgs = {
  id: Scalars["ID"];
};

export type Mutation = {
  __typename?: "Mutation";
  registerAttendee: RegisterAttendeePayload;
  checkInAttendee: CheckInAttendeePayload;
  addSession: AddSessionPayload;
  scheduleSession: ScheduleSessionPayload;
  renameSession: RenameSessionPayload;
  addSpeaker: AddSpeakerPayload;
  modifySpeaker: ModifySpeakerPayload;
  addTrack: AddTrackPayload;
  renameTrack: RenameTrackPayload;
};

export type MutationRegisterAttendeeArgs = {
  input: RegisterAttendeeInput;
};

export type MutationCheckInAttendeeArgs = {
  input: CheckInAttendeeInput;
};

export type MutationAddSessionArgs = {
  input: AddSessionInput;
};

export type MutationScheduleSessionArgs = {
  input: ScheduleSessionInput;
};

export type MutationRenameSessionArgs = {
  input: RenameSessionInput;
};

export type MutationAddSpeakerArgs = {
  input: AddSpeakerInput;
};

export type MutationModifySpeakerArgs = {
  input: ModifySpeakerInput;
};

export type MutationAddTrackArgs = {
  input: AddTrackInput;
};

export type MutationRenameTrackArgs = {
  input: RenameTrackInput;
};

export type Subscription = {
  __typename?: "Subscription";
  onAttendeeCheckedIn: SessionAttendeeCheckIn;
  onSessionScheduled: Session;
};

export type SubscriptionOnAttendeeCheckedInArgs = {
  sessionId: Scalars["ID"];
};

/** The node interface is implemented by entities that have a global unique identifier. */
export type Node = {
  id: Scalars["ID"];
};

/** A connection to a list of items. */
export type AttendeesConnection = {
  __typename?: "AttendeesConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges?: Maybe<Array<AttendeesEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<Attendee>>;
};

export type SessionFilterInput = {
  and?: Maybe<Array<SessionFilterInput>>;
  or?: Maybe<Array<SessionFilterInput>>;
  title?: Maybe<StringOperationFilterInput>;
  abstract?: Maybe<StringOperationFilterInput>;
  startTime?: Maybe<ComparableNullableOfDateTimeOffsetOperationFilterInput>;
  endTime?: Maybe<ComparableNullableOfDateTimeOffsetOperationFilterInput>;
  duration?: Maybe<ComparableTimeSpanOperationFilterInput>;
  sessionSpeakers?: Maybe<ListFilterInputTypeOfSessionSpeakerFilterInput>;
  sessionAttendees?: Maybe<ListFilterInputTypeOfSessionAttendeeFilterInput>;
  track?: Maybe<TrackFilterInput>;
};

export type SessionSortInput = {
  id?: Maybe<SortEnumType>;
  title?: Maybe<SortEnumType>;
  abstract?: Maybe<SortEnumType>;
  startTime?: Maybe<SortEnumType>;
  endTime?: Maybe<SortEnumType>;
  duration?: Maybe<SortEnumType>;
  trackId?: Maybe<SortEnumType>;
  track?: Maybe<TrackSortInput>;
};

/** A connection to a list of items. */
export type SessionsConnection = {
  __typename?: "SessionsConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges?: Maybe<Array<SessionsEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<Session>>;
};

/** A connection to a list of items. */
export type SessionAttendeesConnection = {
  __typename?: "SessionAttendeesConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges?: Maybe<Array<SessionAttendeesEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<Attendee>>;
};

/** A connection to a list of items. */
export type SpeakersConnection = {
  __typename?: "SpeakersConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges?: Maybe<Array<SpeakersEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<Speaker>>;
};

/** A connection to a list of items. */
export type TracksConnection = {
  __typename?: "TracksConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges?: Maybe<Array<TracksEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<Track>>;
};

/** A connection to a list of items. */
export type TrackSessionsConnection = {
  __typename?: "TrackSessionsConnection";
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
  /** A list of edges. */
  edges?: Maybe<Array<TrackSessionsEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<Session>>;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: "PageInfo";
  /** Indicates whether more edges exist following the set defined by the clients arguments. */
  hasNextPage: Scalars["Boolean"];
  /** Indicates whether more edges exist prior the set defined by the clients arguments. */
  hasPreviousPage: Scalars["Boolean"];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars["String"]>;
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars["String"]>;
};

export type Attendee = {
  __typename?: "Attendee";
  id: Scalars["Int"];
  firstName: Scalars["String"];
  lastName: Scalars["String"];
  userName: Scalars["String"];
  emailAddress?: Maybe<Scalars["String"]>;
  country?: Maybe<Scalars["String"]>;
  sessionsAttendees: Array<SessionAttendee>;
};

/** An edge in a connection. */
export type AttendeesEdge = {
  __typename?: "AttendeesEdge";
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
  /** The item at the end of the edge. */
  node: Attendee;
};

export type StringOperationFilterInput = {
  and?: Maybe<Array<StringOperationFilterInput>>;
  or?: Maybe<Array<StringOperationFilterInput>>;
  eq?: Maybe<Scalars["String"]>;
  neq?: Maybe<Scalars["String"]>;
  contains?: Maybe<Scalars["String"]>;
  ncontains?: Maybe<Scalars["String"]>;
  in?: Maybe<Array<Maybe<Scalars["String"]>>>;
  nin?: Maybe<Array<Maybe<Scalars["String"]>>>;
  startsWith?: Maybe<Scalars["String"]>;
  nstartsWith?: Maybe<Scalars["String"]>;
  endsWith?: Maybe<Scalars["String"]>;
  nendsWith?: Maybe<Scalars["String"]>;
};

export type ComparableNullableOfDateTimeOffsetOperationFilterInput = {
  eq?: Maybe<Scalars["DateTime"]>;
  neq?: Maybe<Scalars["DateTime"]>;
  in?: Maybe<Array<Maybe<Scalars["DateTime"]>>>;
  nin?: Maybe<Array<Maybe<Scalars["DateTime"]>>>;
  gt?: Maybe<Scalars["DateTime"]>;
  ngt?: Maybe<Scalars["DateTime"]>;
  gte?: Maybe<Scalars["DateTime"]>;
  ngte?: Maybe<Scalars["DateTime"]>;
  lt?: Maybe<Scalars["DateTime"]>;
  nlt?: Maybe<Scalars["DateTime"]>;
  lte?: Maybe<Scalars["DateTime"]>;
  nlte?: Maybe<Scalars["DateTime"]>;
};

export type ComparableTimeSpanOperationFilterInput = {
  eq?: Maybe<Scalars["TimeSpan"]>;
  neq?: Maybe<Scalars["TimeSpan"]>;
  in?: Maybe<Array<Scalars["TimeSpan"]>>;
  nin?: Maybe<Array<Scalars["TimeSpan"]>>;
  gt?: Maybe<Scalars["TimeSpan"]>;
  ngt?: Maybe<Scalars["TimeSpan"]>;
  gte?: Maybe<Scalars["TimeSpan"]>;
  ngte?: Maybe<Scalars["TimeSpan"]>;
  lt?: Maybe<Scalars["TimeSpan"]>;
  nlt?: Maybe<Scalars["TimeSpan"]>;
  lte?: Maybe<Scalars["TimeSpan"]>;
  nlte?: Maybe<Scalars["TimeSpan"]>;
};

export type ListFilterInputTypeOfSessionSpeakerFilterInput = {
  all?: Maybe<SessionSpeakerFilterInput>;
  none?: Maybe<SessionSpeakerFilterInput>;
  some?: Maybe<SessionSpeakerFilterInput>;
  any?: Maybe<Scalars["Boolean"]>;
};

export type ListFilterInputTypeOfSessionAttendeeFilterInput = {
  all?: Maybe<SessionAttendeeFilterInput>;
  none?: Maybe<SessionAttendeeFilterInput>;
  some?: Maybe<SessionAttendeeFilterInput>;
  any?: Maybe<Scalars["Boolean"]>;
};

export type TrackFilterInput = {
  and?: Maybe<Array<TrackFilterInput>>;
  or?: Maybe<Array<TrackFilterInput>>;
  id?: Maybe<ComparableInt32OperationFilterInput>;
  name?: Maybe<StringOperationFilterInput>;
  sessions?: Maybe<ListFilterInputTypeOfSessionFilterInput>;
};

export enum SortEnumType {
  Asc = "ASC",
  Desc = "DESC",
}

export type TrackSortInput = {
  id?: Maybe<SortEnumType>;
  name?: Maybe<SortEnumType>;
};

export type Session = Node & {
  __typename?: "Session";
  id: Scalars["ID"];
  title: Scalars["String"];
  abstract?: Maybe<Scalars["String"]>;
  startTime?: Maybe<Scalars["DateTime"]>;
  endTime?: Maybe<Scalars["DateTime"]>;
  duration: Scalars["TimeSpan"];
  trackId?: Maybe<Scalars["Int"]>;
  track?: Maybe<Track>;
  speakers: Array<Speaker>;
  attendees?: Maybe<SessionAttendeesConnection>;
};

export type SessionAttendeesArgs = {
  first?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["String"]>;
};

/** An edge in a connection. */
export type SessionsEdge = {
  __typename?: "SessionsEdge";
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
  /** The item at the end of the edge. */
  node: Session;
};

/** An edge in a connection. */
export type SessionAttendeesEdge = {
  __typename?: "SessionAttendeesEdge";
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
  /** The item at the end of the edge. */
  node: Attendee;
};

export type Speaker = Node & {
  __typename?: "Speaker";
  id: Scalars["ID"];
  name: Scalars["String"];
  bio?: Maybe<Scalars["String"]>;
  webSite?: Maybe<Scalars["String"]>;
  sessions: Array<Session>;
  sessionsExpensive: Array<Session>;
  sessionsStream: Array<Session>;
};

/** An edge in a connection. */
export type SpeakersEdge = {
  __typename?: "SpeakersEdge";
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
  /** The item at the end of the edge. */
  node: Speaker;
};

export type Track = Node & {
  __typename?: "Track";
  id: Scalars["ID"];
  name: Scalars["String"];
  sessions?: Maybe<TrackSessionsConnection>;
};

export type TrackSessionsArgs = {
  first?: Maybe<Scalars["Int"]>;
  after?: Maybe<Scalars["String"]>;
  last?: Maybe<Scalars["Int"]>;
  before?: Maybe<Scalars["String"]>;
};

/** An edge in a connection. */
export type TracksEdge = {
  __typename?: "TracksEdge";
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
  /** The item at the end of the edge. */
  node: Track;
};

/** An edge in a connection. */
export type TrackSessionsEdge = {
  __typename?: "TrackSessionsEdge";
  /** A cursor for use in pagination. */
  cursor: Scalars["String"];
  /** The item at the end of the edge. */
  node: Session;
};

export type SessionSpeakerFilterInput = {
  and?: Maybe<Array<SessionSpeakerFilterInput>>;
  or?: Maybe<Array<SessionSpeakerFilterInput>>;
  sessionId?: Maybe<ComparableInt32OperationFilterInput>;
  session?: Maybe<SessionFilterInput>;
  speakerId?: Maybe<ComparableInt32OperationFilterInput>;
  speaker?: Maybe<SpeakerFilterInput>;
};

export type SessionAttendeeFilterInput = {
  and?: Maybe<Array<SessionAttendeeFilterInput>>;
  or?: Maybe<Array<SessionAttendeeFilterInput>>;
  sessionId?: Maybe<ComparableInt32OperationFilterInput>;
  session?: Maybe<SessionFilterInput>;
  attendeeId?: Maybe<ComparableInt32OperationFilterInput>;
  attendee?: Maybe<AttendeeFilterInput>;
};

export type ComparableInt32OperationFilterInput = {
  eq?: Maybe<Scalars["Int"]>;
  neq?: Maybe<Scalars["Int"]>;
  in?: Maybe<Array<Scalars["Int"]>>;
  nin?: Maybe<Array<Scalars["Int"]>>;
  gt?: Maybe<Scalars["Int"]>;
  ngt?: Maybe<Scalars["Int"]>;
  gte?: Maybe<Scalars["Int"]>;
  ngte?: Maybe<Scalars["Int"]>;
  lt?: Maybe<Scalars["Int"]>;
  nlt?: Maybe<Scalars["Int"]>;
  lte?: Maybe<Scalars["Int"]>;
  nlte?: Maybe<Scalars["Int"]>;
};

export type ListFilterInputTypeOfSessionFilterInput = {
  all?: Maybe<SessionFilterInput>;
  none?: Maybe<SessionFilterInput>;
  some?: Maybe<SessionFilterInput>;
  any?: Maybe<Scalars["Boolean"]>;
};

export type SpeakerFilterInput = {
  and?: Maybe<Array<SpeakerFilterInput>>;
  or?: Maybe<Array<SpeakerFilterInput>>;
  id?: Maybe<ComparableInt32OperationFilterInput>;
  name?: Maybe<StringOperationFilterInput>;
  bio?: Maybe<StringOperationFilterInput>;
  webSite?: Maybe<StringOperationFilterInput>;
  sessionSpeakers?: Maybe<ListFilterInputTypeOfSessionSpeakerFilterInput>;
};

export type AttendeeFilterInput = {
  and?: Maybe<Array<AttendeeFilterInput>>;
  or?: Maybe<Array<AttendeeFilterInput>>;
  id?: Maybe<ComparableInt32OperationFilterInput>;
  firstName?: Maybe<StringOperationFilterInput>;
  lastName?: Maybe<StringOperationFilterInput>;
  userName?: Maybe<StringOperationFilterInput>;
  emailAddress?: Maybe<StringOperationFilterInput>;
  country?: Maybe<StringOperationFilterInput>;
  sessionsAttendees?: Maybe<ListFilterInputTypeOfSessionAttendeeFilterInput>;
};

export type RenameSessionInput = {
  sessionId: Scalars["ID"];
  title: Scalars["String"];
};

export type RenameSessionPayload = {
  __typename?: "RenameSessionPayload";
  session?: Maybe<Session>;
  errors?: Maybe<Array<UserError>>;
};

export type ScheduleSessionInput = {
  sessionId: Scalars["ID"];
  trackId: Scalars["ID"];
  startTime: Scalars["DateTime"];
  endTime: Scalars["DateTime"];
};

export type ScheduleSessionPayload = {
  __typename?: "ScheduleSessionPayload";
  track?: Maybe<Track>;
  speakers?: Maybe<Array<Speaker>>;
  session?: Maybe<Session>;
  errors?: Maybe<Array<UserError>>;
};

export type AddSessionInput = {
  title: Scalars["String"];
  abstract?: Maybe<Scalars["String"]>;
  speakerIds: Array<Scalars["ID"]>;
};

export type AddSessionPayload = {
  __typename?: "AddSessionPayload";
  session?: Maybe<Session>;
  errors?: Maybe<Array<UserError>>;
};

export type SessionAttendeeCheckIn = {
  __typename?: "SessionAttendeeCheckIn";
  checkInCount: Scalars["Int"];
  attendee: Attendee;
  session: Session;
  attendeeId: Scalars["ID"];
  sessionId: Scalars["ID"];
};

export type CheckInAttendeeInput = {
  sessionId: Scalars["ID"];
  attendeeId: Scalars["ID"];
};

export type CheckInAttendeePayload = {
  __typename?: "CheckInAttendeePayload";
  session?: Maybe<Session>;
  attendee?: Maybe<Attendee>;
  errors?: Maybe<Array<UserError>>;
};

export type RegisterAttendeeInput = {
  firstName: Scalars["String"];
  lastName: Scalars["String"];
  userName: Scalars["String"];
  emailAddress: Scalars["String"];
};

export type RegisterAttendeePayload = {
  __typename?: "RegisterAttendeePayload";
  attendee?: Maybe<Attendee>;
  errors?: Maybe<Array<UserError>>;
};

export type SessionAttendee = {
  __typename?: "SessionAttendee";
  sessionId: Scalars["Int"];
  session?: Maybe<Session>;
  attendeeId: Scalars["Int"];
  attendee?: Maybe<Attendee>;
};

export type AddSpeakerPayload = {
  __typename?: "AddSpeakerPayload";
  speaker?: Maybe<Speaker>;
  errors?: Maybe<Array<UserError>>;
};

export type AddSpeakerInput = {
  name: Scalars["String"];
  bio?: Maybe<Scalars["String"]>;
  webSite?: Maybe<Scalars["String"]>;
};

export type ModifySpeakerPayload = {
  __typename?: "ModifySpeakerPayload";
  speaker?: Maybe<Speaker>;
  errors?: Maybe<Array<UserError>>;
};

export type ModifySpeakerInput = {
  id: Scalars["ID"];
  name?: Maybe<Scalars["String"]>;
  bio?: Maybe<Scalars["String"]>;
  webSite?: Maybe<Scalars["String"]>;
};

export type AddTrackPayload = {
  __typename?: "AddTrackPayload";
  track?: Maybe<Track>;
  errors?: Maybe<Array<UserError>>;
};

export type AddTrackInput = {
  name: Scalars["String"];
};

export type RenameTrackPayload = {
  __typename?: "RenameTrackPayload";
  track?: Maybe<Track>;
  errors?: Maybe<Array<UserError>>;
};

export type RenameTrackInput = {
  id: Scalars["ID"];
  name: Scalars["String"];
};

export type SessionSpeaker = {
  __typename?: "SessionSpeaker";
  sessionId: Scalars["Int"];
  session?: Maybe<Session>;
  speakerId: Scalars["Int"];
  speaker?: Maybe<Speaker>;
};

export type UserError = {
  __typename?: "UserError";
  message: Scalars["String"];
  code: Scalars["String"];
};

export type Unnamed_1_QueryVariables = Exact<{
  [key: string]: never;
}>;

export type Unnamed_1_Query = {
  __typename?: "Query";
  sessions?:
    | {
        __typename?: "SessionsConnection";
        nodes?:
          | Array<{
              __typename?: "Session";
              id: string;
              abstract?: string | null | undefined;
              title: string;
            }>
          | null
          | undefined;
      }
    | null
    | undefined;
};

export const Document = gql`
  {
    sessions {
      nodes {
        id
        abstract
        title
      }
    }
  }
`;

/**
 * __useQuery__
 *
 * To run a query within a React component, call `useQuery` and pass it any options that fit your needs.
 * When your component renders, `useQuery` returns an object from Apollo Client that contains loading, error, and data properties
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useQuery({
 *   variables: {
 *   },
 * });
 */

/*
export function useQuery(baseOptions?: Apollo.QueryHookOptions<Query, QueryVariables>) {
        const options = {...defaultOptions, ...baseOptions}
        return Apollo.useQuery<Query, QueryVariables>(Document, options);
      }
export function useLazyQuery(baseOptions?: Apollo.LazyQueryHookOptions<Query, QueryVariables>) {
          const options = {...defaultOptions, ...baseOptions}
          return Apollo.useLazyQuery<Query, QueryVariables>(Document, options);
        }
export type QueryHookResult = ReturnType<typeof useQuery>;
export type LazyQueryHookResult = ReturnType<typeof useLazyQuery>;
export type QueryResult = Apollo.QueryResult<Query, QueryVariables>;
*/
